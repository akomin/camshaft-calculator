package view;

import javax.swing.*;

import java.awt.*;

public class ImagePanel extends JPanel {

	private static final long serialVersionUID = 1L;
	JLabel imageLabel;
	private Image image;
	
	public ImagePanel(String imagePath) {
		imageLabel = new JLabel(new ImageIcon(getClass().getResource(imagePath)));
		this.setLayout(new BorderLayout());
		this.add(imageLabel, BorderLayout.CENTER);
	}
	public Image getImage() {
		return image;
	}
	
	public void setImage(String imagePath) {
		imageLabel.setIcon(new ImageIcon(getClass().getResource(imagePath)));
		repaint();
	}	
}