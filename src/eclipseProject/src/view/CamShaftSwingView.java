package view;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.HashMap;

import controller.SwingViewController;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import model.CamShaftType;

	
public class CamShaftSwingView extends JFrame{
	
	private static final long serialVersionUID = 1564127585051330410L;
	private ImagePanel imagePanel;
	private HashMap<String, Double> params;
	MotionLawPanel motionPanel;
	
	SwingViewController controller;
	
	public CamShaftSwingView() {
		super("CamShaft profile calculator");
		
		setBounds(200, 100, 1200, 800); 
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		final JTabbedPane jtp = new JTabbedPane();
		getContentPane().add(jtp);
		
		JPanel panel_parameters = new JPanel();
		panel_parameters.setLayout(new BorderLayout(5,5));
		
		JPanel panel_trajectory = new JPanel();
		panel_trajectory.setLayout(new BorderLayout());
		
		JPanel panel_profile = new JPanel();
		panel_profile.setLayout(new BorderLayout());
		
		 jtp.addTab("Parameters", panel_parameters);

	     jtp.addTab("Movement law", panel_trajectory);
	     
	     jtp.addTab("Profile", panel_profile);
	     
	     jtp.setEnabledAt(1, false);
	     jtp.setEnabledAt(2, false);
	     
		JPanel parameters_panelLeft = new JPanel();
		parameters_panelLeft.setLayout(new BorderLayout(4,4));
	    panel_parameters.add(parameters_panelLeft, BorderLayout.WEST);
	    
	    JPanel parameters_panelLeft_down = new JPanel();
	    parameters_panelLeft_down.setLayout(new GridLayout(1,2));
	    parameters_panelLeft.setMaximumSize(new Dimension(300,500));
	    parameters_panelLeft_down.setMaximumSize(new Dimension(300,400));
		
	    JPanel parameters_panelLeft_down_L = new JPanel();
	    parameters_panelLeft_down_L.setLayout(new ColumnLayout(2,2,6,ColumnLayout.LEFT));
	    JPanel parameters_panelLeft_down_R = new JPanel();
	    parameters_panelLeft_down_R.setLayout(new ColumnLayout(2,2,2,ColumnLayout.LEFT));
	    		
		final JLabel label_X = new JLabel("X =");
		final JLabel label_Y = new JLabel("Y =");
		final JLabel label_L = new JLabel("L =");
		final JLabel label_d = new JLabel("d =");
		final JLabel label_Rmin = new JLabel("Rmin =");
		final JLabel label_Prec = new JLabel("Precision =");
		
		final JTextField textField_X = new JTextField("130", 5);
		final JTextField textField_Y = new JTextField("100", 5);
		final JTextField textField_L = new JTextField("100",5);
		final JTextField textField_d = new JTextField("15",5);
		final JTextField textField_Rmin = new JTextField("80",5);
		final JTextField textField_Prec = new JTextField("1",5);
		
		CamShaftType[] camShaftTypes = {CamShaftType.OSCILATING, CamShaftType.TRANSICTING};
		final JComboBox<CamShaftType> type_chooser = new JComboBox<CamShaftType>(camShaftTypes);
		type_chooser.setSelectedIndex(0);
		type_chooser.addActionListener(new ActionListener() {


			@Override
			public void actionPerformed(ActionEvent ev) {
				JComboBox<CamShaftType> box = (JComboBox<CamShaftType>)ev.getSource();
				CamShaftType type = (CamShaftType)box.getSelectedItem();
				
				switch(type) {
				case TRANSICTING :
					label_Y.setVisible(false);
					textField_Y.setVisible(false);
					label_L.setVisible(false);
					textField_L.setVisible(false);
					label_Y.setVisible(false);

					imagePanel.setImage("/CamShaft_Type2.gif");
				
					break;
				case OSCILATING :	
					label_Y.setVisible(true);
					textField_Y.setVisible(true);
					label_L.setVisible(true);
					textField_L.setVisible(true);
					label_Y.setVisible(true);
					
					imagePanel.setImage("/CamShaft_Type1.gif");
				
					break;
					
				}
				
			}
		});
		
		parameters_panelLeft.add(type_chooser,BorderLayout.NORTH );
		parameters_panelLeft.add( new JScrollPane(parameters_panelLeft_down), BorderLayout.CENTER);
		parameters_panelLeft_down_L.add(label_X);
		parameters_panelLeft_down_R.add(textField_X);
		parameters_panelLeft_down_L.add(label_d);
		parameters_panelLeft_down_R.add(textField_d);
		parameters_panelLeft_down_L.add(label_Rmin);
		parameters_panelLeft_down_R.add(textField_Rmin);
		parameters_panelLeft_down_L.add(label_Prec);
		parameters_panelLeft_down_R.add(textField_Prec);
		parameters_panelLeft_down_L.add(label_Y);
		parameters_panelLeft_down_R.add(textField_Y);
		parameters_panelLeft_down_L.add(label_L);
		parameters_panelLeft_down_R.add(textField_L);
		
		JButton apply_button = new JButton("Apply");
		apply_button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				params = new HashMap<String, Double>();
				params.put("X", Double.parseDouble(textField_X.getText()));
				params.put("d", Double.parseDouble(textField_d.getText()));
				params.put("Rmin", Double.parseDouble(textField_Rmin.getText()));
				params.put("Prec", Double.parseDouble(textField_Prec.getText()));
				params.put("Y", Double.parseDouble(textField_Y.getText()));
				params.put("L", Double.parseDouble(textField_L.getText()));
				
				controller = new SwingViewController((CamShaftType)type_chooser.getSelectedItem(), params);
				motionPanel.init(controller);
			    jtp.setEnabledAt(1, true);
			    jtp.setEnabledAt(2, true);
			}
		});
		
		parameters_panelLeft_down_L.add(apply_button);
		
		parameters_panelLeft_down.add(parameters_panelLeft_down_L);
		parameters_panelLeft_down.add(parameters_panelLeft_down_R);
		
		JPanel parameters_imagePanel = new JPanel();
		parameters_imagePanel.setLayout(new BorderLayout());
		

		imagePanel = new ImagePanel("/CamShaft_Type1.gif");
		parameters_imagePanel.add(imagePanel, BorderLayout.CENTER);
		
		parameters_imagePanel.setMaximumSize(parameters_imagePanel.getMinimumSize());
		
		panel_parameters.add(parameters_imagePanel);
		
		motionPanel = new MotionLawPanel();
		panel_trajectory.add(motionPanel);
		
		final CamGraphic profileGraph = new CamGraphic();
		profileGraph.setVisible(true);
		panel_profile.add(profileGraph, BorderLayout.CENTER);
		panel_profile.setVisible(true);
		
	     jtp.addChangeListener(new ChangeListener() {
				
			@Override
			public void stateChanged(ChangeEvent arg0) {
				int index = jtp.getSelectedIndex();
				
				switch(index) {

				case(2) :
					
					controller.calculateProfile(Double.parseDouble(textField_Prec.getText()));
					profileGraph.setPointData(controller.getProfile());
					break;
				}
				
			}
		});
	}
	
	public static void main(String[] args) {
		CamShaftSwingView view = new CamShaftSwingView();
		view.setVisible(true);
	}

}
