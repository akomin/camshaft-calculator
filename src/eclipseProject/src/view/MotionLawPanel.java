package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import model.Point;
import model.utils.FunctionEvaluator;
import controller.SwingViewController;

public class MotionLawPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final CamGraphic motionLawGraphic;
	private JDialog functionDialog;
	SwingViewController controller;

	public MotionLawPanel() {
		super();
		motionLawGraphic = new CamGraphic();
		motionLawGraphic.allowPointMarkers(true);
		motionLawGraphic.setVisible(true);
		
		this.setLayout(new BorderLayout());
		
		JPanel panel_menu = new JPanel();
		panel_menu.setLayout(new ColumnLayout(2, 2, 3, ColumnLayout.LEFT));

		JPanel panel_menu_buttons = new JPanel();

		JPanel panel_Graph = new JPanel();
		panel_Graph.setLayout(new BorderLayout());

		JButton openButton = new JButton("Open");
		openButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fileopen = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter(
						"*.TXT", "txt");
				fileopen.setFileFilter(filter);
				int ret = fileopen.showDialog(null, "Открыть файл");
				if (ret == JFileChooser.APPROVE_OPTION) {
					File file = fileopen.getSelectedFile();
					try {
						MotionLawPanel.this.controller.loadTrajectory(file);
						motionLawGraphic
								.setPointData(MotionLawPanel.this.controller
										.getTrajectory());
						motionLawGraphic.repaint();

					} catch (NumberFormatException e) {
						JOptionPane.showMessageDialog(MotionLawPanel.this,
								"Wrong coordinate format!",
								"Trajectory loading error",
								JOptionPane.ERROR_MESSAGE);
					} catch (ParseException e) {
						JOptionPane.showMessageDialog(MotionLawPanel.this,
								"Wrong file format!",
								"Trajectory loading error",
								JOptionPane.ERROR_MESSAGE);
					} catch (IOException e) {
						JOptionPane.showMessageDialog(MotionLawPanel.this,
								"Error occured while file loading!",
								"Trajectory loading error",
								JOptionPane.ERROR_MESSAGE);
					}
				}

			}
		});

		JButton saveButton = new JButton("Save");
		saveButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				FileNameExtensionFilter filter = new FileNameExtensionFilter(
						"*.TXT", "txt");
				JFileChooser fc = new JFileChooser();

				fc.setFileFilter(filter);
				if (fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
					try {
						MotionLawPanel.this.controller.saveTrajectory(fc
								.getSelectedFile().getAbsolutePath());
					} catch (IOException e) {
						JOptionPane.showMessageDialog(MotionLawPanel.this,
								"Unable to save trajectory to file!",
								"File saving error", JOptionPane.ERROR_MESSAGE);
					}
				}

			}
		});

		JButton setFunctionButton = new JButton("Function");
		setFunctionButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				functionDialog = new JDialog();
				JPanel p1 = new JPanel();
				JPanel p2 = new JPanel();
				JPanel p3 = new JPanel();
				functionDialog.add(p1, BorderLayout.NORTH);
				functionDialog.add(p2, BorderLayout.CENTER);
				functionDialog.add(p3, BorderLayout.SOUTH);
				final JTextField x1 = new JTextField(
						"(sin(x/180*pi)^2-sin(x/180*pi))*50");
				final JTextField x2 = new JTextField("0");
				final JTextField y1 = new JTextField("360");
				final JTextField y2 = new JTextField("10");

				p1.add(new JLabel("Y(x)=", JLabel.RIGHT));
				p1.add(x1);
				p2.add(new JLabel("Start x=", JLabel.RIGHT));
				p2.add(x2);
				p2.add(new JLabel("End x=", JLabel.RIGHT));
				p2.add(y1);
				p2.add(new JLabel("Step x=", JLabel.RIGHT));
				p2.add(y2);
				JButton ok = new JButton("OK");
				functionDialog.pack();
				functionDialog.setLocationRelativeTo(null);
				ok.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						String expr = x1.getText();
						int startX = Integer.parseInt(x2.getText());
						int endX = Integer.parseInt(y1.getText());
						int step = Integer.parseInt(y2.getText());
						
						String expression = "3 + 4 * 2 / ( 1 - 5 ) ^ 2 ^ 3";
						String rpn = FunctionEvaluator.sortingStation(expression);
						System.out.println(rpn);
						
						String rpnExpr = FunctionEvaluator.sortingStation(expr);
						ArrayList<Point> pointsData = new ArrayList<Point>();
						for (int x = startX; x <= endX; x = x + step) {
							pointsData.add(new Point(x, FunctionEvaluator.eval(
									rpnExpr, x / 1.0)));
						}

						MotionLawPanel.this.controller
								.setTrajectory(pointsData);
						motionLawGraphic
								.setPointData(MotionLawPanel.this.controller
										.getTrajectory());
						functionDialog.setVisible(false);
						motionLawGraphic.repaint();

					}
				});
				JButton cancel = new JButton("Cancel");
				cancel.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						functionDialog.setVisible(false);

					}
				});
				p3.add(ok);
				p3.add(cancel);
				functionDialog.setSize(new Dimension(400, 150));
				functionDialog.setVisible(true);
			}
		});

		this.add(panel_menu, BorderLayout.EAST);
		this.add(panel_Graph, BorderLayout.CENTER);

		panel_menu.add(panel_menu_buttons, BorderLayout.NORTH);
		panel_menu_buttons.setLayout(new GridLayout(3, 1));

		panel_menu_buttons.add(openButton);
		panel_menu_buttons.add(saveButton);
		panel_menu_buttons.add(setFunctionButton);

		panel_Graph.add(motionLawGraphic, BorderLayout.CENTER);

	}

	public void init(SwingViewController controller) {

		this.controller = controller;
		motionLawGraphic
				.setPointData(MotionLawPanel.this.controller
						.getTrajectory());
		motionLawGraphic.repaint();
		repaint();
	}

}
