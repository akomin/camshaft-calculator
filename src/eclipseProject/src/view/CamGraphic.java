package view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.util.ArrayList;

import model.*;

import javax.swing.JPanel;

public class CamGraphic extends JPanel {

	private static final long serialVersionUID = -2055830646182448946L;

	private ArrayList<Point> pointsData;
	private double max_x = 0;
	private double max_y = 0;
	private double min_x = 0;
	private double min_y = 0;
	private int width = 0;
	private int height = 0;
	private double k_x = 0;
	private double k_y = 0;
	private double dw = 0;
	private double dh = 0;
	private double text_dw = 0;
	private double text_dh = 0;
	public boolean drawPointMarkers = false;

	public CamGraphic() {
		super();
		pointsData = new ArrayList<Point>();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;

		max_x = Double.MIN_VALUE;
		max_y = Double.MIN_VALUE;
		min_x = Double.MAX_VALUE;
		min_y = Double.MAX_VALUE;

		width = this.getSize().width - 2;
		height = this.getSize().height - 2;

		FontMetrics fm = g.getFontMetrics();
		text_dw = fm.stringWidth("0.0000001") + 1;
		text_dh = fm.getHeight() * 2 - 1;

		for (int i = 0; i < pointsData.size(); i = i + 1) {
			Point p = pointsData.get(i);
			max_x = Math.max(p.x, max_x);
			max_y = Math.max(p.y, max_y);
			min_x = Math.min(p.x, min_x);
			min_y = Math.min(p.y, min_y);
		}

		double offset = 0;
		if (((width - text_dw) / (max_x - min_x)) > ((height - text_dh) / (max_y - min_y))) {
			offset = ((width - text_dw) * (max_y - min_y) / (height - text_dh) - (max_x - min_x)) / 2;
			max_x = max_x + offset;
			min_x = min_x - offset;
		} else if (((width - text_dw) / (max_x - min_x)) < ((height - text_dh) / (max_y - min_y))) {
			offset = ((height - text_dh) * (max_x - min_x) / (width - text_dw) - (max_y - min_y)) / 2;
			max_y = max_y + offset;
			min_y = min_y - offset;
		}

		k_x = (width - text_dw) / (max_x - min_x);
		k_y = (height - text_dh) / (max_y - min_y);

		dw = -min_x * k_x;
		dh = -min_y * k_y;

		g.drawRect(1, 1, width, height);

		g.setColor(Color.gray);
		
		//prints y сoordinate grid from 0 to min y
		int step = (int) Math.abs((max_y - min_y) / 40);
		int j = 0;
		int i = 0;
		while (j <= max_y && min_y != max_y) {
			if (j >= min_y && j <= max_y) {
				g2.setColor(Color.gray);
				g2.draw(new Line2D.Double(text_dw, height - text_dh - (int) Math.round(j * k_y + dh), width, height
						- text_dh - (int) Math.round(j * k_y + dh)));
				if (i == 0) {
					g2.setColor(Color.black);
					double yi = height - text_dh - Math.round(j * k_y + dh - text_dh / 5);
					if (yi >= text_dh / 2) {
						g2.drawString(j + "", 5, (int) yi);
					}
					g2.draw(new Line2D.Double(text_dw, height - text_dh - Math.round(j * k_y + dh),
							width, height - text_dh - (int) Math.round(j * k_y + dh)));
					i = i + 1;
				} else {
					i = 0;
				}
			}
			j = j + step;
		}
		
		//prints y сoordinate grid from 0 to max y
	      j=0; i=0;
	      while(j>=min_y && min_y!=max_y){
	         if(j>=min_y && j<=max_y){
	        	g2.setColor(Color.gray);
	            g2.draw(new Line2D.Double(text_dw,height-text_dh-(int)Math.round(j*k_y+dh),width,height-text_dh-(int)Math.round(j*k_y+dh)));
	            if(i==0){
	               g2.setColor(Color.black);
	               g2.drawString(j+"",5,(int)(height-text_dh-(int)Math.round(j*k_y+dh-text_dh/5)));
	               g2.draw(new Line2D.Double(text_dw-5,height-text_dh-(int)Math.round(j*k_y+dh),width,height-text_dh-(int)Math.round(j*k_y+dh)));
	               i=i+1;}else{
	               i=0;
	            }
	         }
	         j=j-step;
	      }
	      
	    //prints x сoordinate grid from 0 to max x
	      step= (int)Math.abs((max_x-min_x)/60);
	      j=0; i=0;
	      String st;
	      while(j<=max_x && min_x!=max_x){
	         if(j>=min_x && j<=max_x){
	            g2.setColor(Color.gray);
	            g2.draw(new Line2D.Double(text_dw+(int)Math.round(j*k_x+dw),1,text_dw+(int)Math.round(j*k_x+dw),height-text_dh));
	            if(i==0){
	               g2.setColor(Color.black);

	               int xi = (int)(text_dw+(int)Math.round(j*k_x+dw-fm.stringWidth(j+"")/2f));
	                  g2.drawString(j+"",xi,(int)(height-text_dh+5+text_dh*0.5f));

	               g2.draw(new Line2D.Double(text_dw+(int)Math.round(j*k_x+dw),1,text_dw+(int)Math.round(j*k_x+dw),height-text_dh+5));
	               i=i+1;
	              } else {
	               i=0;
	            }
	         }
	         j=j+step;
	      }
	      
	      //prints x сoordinate grid from 0 to min x
	      j=0; i=0;
	      while(j>=min_x && min_x!=max_x){
	         if(j>=min_x && j<=max_x){
	        	g2.setColor(Color.gray);
	            g2.draw(new Line2D.Double(text_dw+(int)Math.round(j*k_x+dw),1,text_dw+(int)Math.round(j*k_x+dw),height-text_dh));
	            if(i==0){
	               g2.setColor(Color.black);
	               st = j+"";
	               g2.drawString(st,(int)(text_dw+(int)Math.round(j*k_x+dw-fm.stringWidth(st)/2f)),(int)(height-text_dh+5+text_dh*0.5f));
	               g2.draw(new Line2D.Double(text_dw+(int)Math.round(j*k_x+dw),1,text_dw+(int)Math.round(j*k_x+dw),height-text_dh+5));
	               i=i+1;
	            }else {
	               i = 0;
	            }
	         }
	         j=j-step;
	      }
	      
	    //prints motion law graphic curve 
	    g2.setColor(Color.black);
		for ( i = 0, j = 1; j <= pointsData.size() - 1; i++, j++) {
			Point p1 = pointsData.get(i);
			Point p2 = pointsData.get(j);
			Stroke stroke = new BasicStroke(2f);
			g2.setStroke(stroke);

			g2.draw(new Line2D.Double(text_dw + p1.x * k_x + dw,
					height - text_dh - p1.y * k_y - dh, text_dw + p2.x * k_x + dw, height - text_dh - p2.y
							* k_y - dh));
			
			//prints motion law point positions
			if (drawPointMarkers) {
				stroke = new BasicStroke(1f);
				g2.setStroke(stroke);

				g2.fill(new Ellipse2D.Double(text_dw + p1.x * k_x + dw - 5.0, height - text_dh - p1.y * k_y - dh-5.0, 
						10.0, 10.0));

			}
		}

	}

	public void setPointData(ArrayList<Point> points) {
		this.pointsData = points;

		repaint();

	}

	public void allowPointMarkers(boolean value) {
		this.drawPointMarkers = value;
		repaint();
	}

}
