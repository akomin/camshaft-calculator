package tests;

import java.util.ArrayList;
import java.util.HashMap;

import model.*;

import org.junit.Test;

public class CamShaftTest {
	private ArrayList<Point> trajectory;
	private HashMap<String, Double> oscilatingProps;
	private void initTrajectory() {
		trajectory = new ArrayList<Point>();
		oscilatingProps = new HashMap <String, Double>();
		
		trajectory.add(new Point(0.0, 0.0));
		trajectory.add(new Point(20.0, 0.0));
		trajectory.add(new Point(25.20,1.534));
		trajectory.add(new Point(30.0, 1.534));
		trajectory.add(new Point(42.4,0.0));
		trajectory.add(new Point(55.3,0.0));
		trajectory.add(new Point(62.41, 1.534));
		trajectory.add(new Point(68.21, 1.534));
		trajectory.add(new Point(84.11,0.0));
		trajectory.add(new Point(92.45,0.0));
		trajectory.add(new Point(102.654,1.534));
		trajectory.add(new Point(108.123,1.534));
		trajectory.add(new Point(115.232,0.0));
		trajectory.add(new Point(127.432,0.0));
		trajectory.add(new Point(134.323, 1.534));
		trajectory.add(new Point(143.421, 1.534));
		trajectory.add(new Point(154.142,0.0));
		trajectory.add(new Point(162.223,0.0));
		trajectory.add(new Point(174.897,1.534));
		trajectory.add(new Point(182.922,1.534));
		trajectory.add(new Point(190.232,0.0));
		trajectory.add(new Point(199.922,0.0));
		trajectory.add(new Point(204.472,1.534));
		trajectory.add(new Point(213.485,1.534));
		trajectory.add(new Point(223.653,0.0));
		trajectory.add(new Point(230.965,0.0));
		trajectory.add(new Point(244.865,1.534));
		trajectory.add(new Point(253.321,1.534));
		trajectory.add(new Point(263.953,0.0));
		trajectory.add(new Point(274.312,0.0));
		trajectory.add(new Point(283.421,1.534));
		trajectory.add(new Point(291.082,1.534));
		trajectory.add(new Point(303.253,0.0));
		trajectory.add(new Point(310.362,0.0));
		trajectory.add(new Point(318.452,1.534));
		trajectory.add(new Point(325.532,1.534));
		trajectory.add(new Point(332.132,0.0));
		trajectory.add(new Point(339.565,0.0));
		trajectory.add(new Point(346.293,1.534));
		trajectory.add(new Point(351.391,1.534));
		trajectory.add(new Point(356.221,0.0));
		trajectory.add(new Point(360.0,0.0));
		
		oscilatingProps.put("X", 130.0);
		oscilatingProps.put("Y", 110.0);
		oscilatingProps.put("L", 110.0);
		oscilatingProps.put("d", 15.0);
		oscilatingProps.put("Rmin", 80.0);
		
	}
	@Test
	public void testCamshaft1() {
		initTrajectory();
		CamShaft camShaft1 = CamShaftFactory.create(CamShaftType.OSCILATING, oscilatingProps);		
		camShaft1.setTrajectory(trajectory);
		camShaft1.calculateProfile(1.0);
		
		ArrayList<Point> profile = camShaft1.getProfile();
		System.out.println("fin");
	}

}
