package tests;

import static org.junit.Assert.*;

import org.junit.Test;
import model.utils.FunctionEvaluator;;

public class FunctionEvaluatorTest {
	
	@Test
	public void testRPNConvertion1() {
		String expression = "-1";
		String rpn = FunctionEvaluator.sortingStation(expression);
		assertEquals("1 -", rpn);
	}
	
	@Test
	public void testRPNConvertion2() {
		String expression = "7-2*3";
		String rpn = FunctionEvaluator.sortingStation(expression);
		assertEquals("7 2 3 * -", rpn);
	}
	
	@Test
	public void testRPNConvertion3() {
		String expression = "8 + (5*2/(3+1))*2";
		String rpn = FunctionEvaluator.sortingStation(expression);
		assertEquals("8 5 2 * 3 1 + / 2 * +", rpn);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testRPNConvertion4() {
		String expression = ") + 1";
		String rpn = FunctionEvaluator.sortingStation(expression);
	}
	
	@Test(expected = IllegalStateException.class)
	public void testRPNConvertion5() {
		String expression = "";
		String rpn = FunctionEvaluator.sortingStation(expression);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testRPNConvertion6() {
		String expression = "3 + (2+4))";
		String rpn = FunctionEvaluator.sortingStation(expression);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testRPNConvertion7() {
		String expression = "3 + ((2+4)";
		String rpn = FunctionEvaluator.sortingStation(expression);
	}
	
	@Test
	public void testEval1() {
		String expression = "4 + 3";
		String rpn = FunctionEvaluator.sortingStation(expression);
		
		assertEquals(7, FunctionEvaluator.eval(rpn), 0.000000001);
	}
	
	@Test
	public void testEval2() {
		String expression = "8 + (5*2/(3+1))*2";
		String rpn = FunctionEvaluator.sortingStation(expression);
		
		assertEquals(8 + (5*2.0/(3+1))*2, FunctionEvaluator.eval(rpn), 0.000000001);
	}
	
	@Test
	public void testEval3() {
		String expression = "sin(pi/4)^2 + cos(pi/4)^2";
		String rpn = FunctionEvaluator.sortingStation(expression);
		
		assertEquals(Math.pow(Math.sin(Math.PI/4),2) + Math.pow(Math.cos(Math.PI/4),2), FunctionEvaluator.eval(rpn), 0.000000001);
	}
	
	@Test
	public void testEval4() {
		String expression = "sin(x)^2 + cos(x)^2";
		String rpn = FunctionEvaluator.sortingStation(expression);
		
		assertEquals(Math.pow(Math.sin(Math.PI/4),2) + Math.pow(Math.cos(Math.PI/4),2), FunctionEvaluator.eval(rpn, Math.PI/2), 0.000000001);
	}
	
}
