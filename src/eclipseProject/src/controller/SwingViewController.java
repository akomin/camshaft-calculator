package controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import model.CamShaft;
import model.CamShaftFactory;
import model.CamShaftType;
import model.Point;

public class SwingViewController {
	
	private CamShaft camShaft;
	
	public SwingViewController(CamShaftType type, HashMap<String, Double> props) {
		camShaft = CamShaftFactory.create(type, props);
	}
	
	public ArrayList<Point> getTrajectory() {
		return camShaft.getTrajectory();
	}
	
	public void setTrajectory(ArrayList<Point> trajectory) {
		camShaft.setTrajectory(trajectory);
	}
	
	public ArrayList<Point> getProfile() {
		return camShaft.getProfile();
	}
	
	public void calculateProfile(Double pres) {
		camShaft.calculateProfile(pres);
	}
	
	public void loadTrajectory(File file) throws
			ParseException, NumberFormatException, IOException {

		String line = "";
		camShaft.setTrajectory(new ArrayList<Point>());
		double x;
		double y;

		ArrayList<Point> trajectory = new ArrayList<Point>();

		BufferedReader buffReader = null;
		try {
			buffReader = new BufferedReader(new FileReader(file));
	
		while ((line = buffReader.readLine()) != null) {
			String[] coordinates = line.split(",");

			if (coordinates.length == 0) {
				try {
					buffReader.close();
				} catch (IOException e) {
					throw new IOException("File reading buffer closing error");
				}
				throw new ParseException("Wrong file format!", 0);
			}
			x = Double.parseDouble(coordinates[0]);
			y = Double.parseDouble(coordinates[1]);

			trajectory.add(new Point(x, y));

		}
		
		} catch (FileNotFoundException e) {
			throw new FileNotFoundException("File not found!");
		}
		
		camShaft.setTrajectory(trajectory);
		
		try {
			buffReader.close();
		} catch (IOException e) {
			throw new IOException("File reading buffer closing error");
		}
	}


	public void saveTrajectory(String fileName)
			throws UnsupportedEncodingException, IOException,
			FileNotFoundException {

		ArrayList<Point> trajectory = camShaft.getTrajectory();
		FileOutputStream fileOutputStream = null;
		BufferedWriter bufferedWriter = null;

		fileOutputStream = new FileOutputStream(fileName);
		bufferedWriter = new BufferedWriter(new OutputStreamWriter(
				fileOutputStream, "UTF-8"));
		for (Point point : trajectory) {
			bufferedWriter.write(String.valueOf(point.x) + ","
					+ String.valueOf(point.y));
			bufferedWriter.write("\n");
		}

		bufferedWriter.flush();

		bufferedWriter.close();

		fileOutputStream.close();

	}

}
	

