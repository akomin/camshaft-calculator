package model;

/**
 * The enumeration contains the types of cams, which profile 
 * can be calculated in CamShaft application
 */
public enum CamShaftType {
	
	TRANSICTING, 
	OSCILATING

}
