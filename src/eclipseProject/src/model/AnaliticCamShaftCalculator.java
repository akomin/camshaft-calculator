package model;

import java.util.ArrayList;

/**
 * Contains general logic for analitical profile calculation methods
 *
 */
public abstract class AnaliticCamShaftCalculator implements CamShaftCalculator {

	protected double Fl0;
	protected double Rmin;
	protected double l;
	protected double L;
	protected double X;
	protected double Y;
	protected double d;
	
	@Override
	
	/*
	 * (non-Javadoc)
	 * @see model.CamShaftCalculator#calculateProfile(java.util.ArrayList, double)
	 */
	public ArrayList<Point> calculateProfile(ArrayList<Point> trajectory,
			double pres) {

		ArrayList<Point> profile = new ArrayList<Point>();
		if (trajectory.size() == 0) {
			return profile; 
		}
		l = getVectorLenght(new Point(0.0, 0.0), new Point(X, Y));
		Fl0 = getFl0(l);
		double Fn = getFn(Rmin);

		double x = trajectory.get(0).x;

		for (int i = 0; i < trajectory.size() - 1; i++) {
			Point p1 = trajectory.get(i);
			Point p2 = trajectory.get(i + 1);

			// y = kx+b line
			double k = 0;
			if (p2.y != p1.y) {
				k = (p2.y - p1.y) / (p2.x - p1.x);
			}
			double b = p1.y - k * p1.x;

			while (x <= p2.x) {
				double Ri = getRi(k * x + b);
				double Fi = x * Math.PI / 180;
				profile.add(new Point(Ri * Math.cos(Fi + Fn), -1 * Ri
						* Math.sin(Fi + Fn)));
				x = x + pres;
			}
		}
		return profile;
	}
		
		protected double getFl0(double l) {
			return Math.acos((l*l + Rmin*Rmin - L*L)/(2*l*Rmin));
		}
		
		protected double getVectorLenght(Point p1, Point p2) {
			return Math.sqrt(Math.pow(p1.x-p2.x, 2) + Math.pow(p1.y-p2.y, 2));
		}
		
		protected abstract double getFn(double r);
		
		protected abstract double getRi(double Sbi);
		
		/**
		 * @return the rmin
		 */
		public double getRmin() {
			return Rmin;
		}

		/**
		 * @param rmin the rmin to set
		 */
		public void setRmin(double rmin) {
			Rmin = rmin;
		}

		/**
		 * @return the l
		 */
		public double getL() {
			return L;
		}

		/**
		 * @param l the l to set
		 */
		public void setL(double l) {
			L = l;
		}

		/**
		 * @return the x
		 */
		public double getX() {
			return X;
		}

		/**
		 * @param x the x to set
		 */
		public void setX(double x) {
			X = x;
		}

		/**
		 * @return the y
		 */
		public double getY() {
			return Y;
		}

		/**
		 * @param y the y to set
		 */
		public void setY(double y) {
			Y = y;
		}
}
