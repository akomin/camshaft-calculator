package model;

import java.util.HashMap;

/**
 * Builds CamShaft objects with CamShaftType.OSCILATING type and specific profile calculator class
 * OsciliatingCamShaftCalculator
 */
public class OsciliatingCamShaftBuilder extends CamShaftBuilder {

	@Override
	public void build(HashMap<String, Double> props) {
		camshaft = new CamShaft(CamShaftType.OSCILATING);
		camshaft.setCamshaftCalculator(new OsciliatingCamShaftCalculator(props));
	}

	@Override
	public void reinit(CamShaft camshaft, HashMap<String, Double> props) {
		camshaft.setCamshaftCalculator(new OsciliatingCamShaftCalculator(props));
	}

}
