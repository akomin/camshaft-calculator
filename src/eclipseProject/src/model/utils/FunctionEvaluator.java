package model.utils;

import java.util.*;


/**
 * Provides functionality to convert string math-expression to java expressions that can be
 * evaluated (with substitution of parameters or without it)
 */
public class FunctionEvaluator {

	/**
	 * Main math operations with declared priority
	 * 
	 */
	public static final Map<String, Integer> MAIN_MATH_OPERATIONS;

	static {
		MAIN_MATH_OPERATIONS = new HashMap<String, Integer>();
		MAIN_MATH_OPERATIONS.put("*", 1);
		MAIN_MATH_OPERATIONS.put("/", 1);
		MAIN_MATH_OPERATIONS.put("sin", 1);
		MAIN_MATH_OPERATIONS.put("cos", 1);
		MAIN_MATH_OPERATIONS.put("tg", 1);
		MAIN_MATH_OPERATIONS.put("ctg", 1);
		MAIN_MATH_OPERATIONS.put("^", 1);
		MAIN_MATH_OPERATIONS.put("+", 2);
		MAIN_MATH_OPERATIONS.put("-", 2);
	}

	/**
	 * @param leftBracket
	 *            opening bracket.
	 * @param rightBracket
	 *            closing bracket.
	 * @return expression converted to reverse polish notation
	 * @throws IllegalArgumentException
	 */
	public static String sortingStation(String expression,
			Map<String, Integer> operations, String leftBracket,
			String rightBracket) {
		if (expression == null || expression.length() == 0)
			throw new IllegalStateException("Expression isn't specified.");
		if (operations == null || operations.isEmpty())
			throw new IllegalStateException("Operations aren't specified.");

		expression = expression.replaceAll("e", "" + Math.E);
		expression = expression.replaceAll("pi", "" + Math.PI);

		// tokens (operands and operators) from expression string
		List<String> out = new ArrayList<String>();

		Stack<String> operationsStack = new Stack<String>();

		expression = expression.replace(" ", "");

		// non-operation symbols
		Set<String> operationSymbols = new HashSet<String>(operations.keySet());
		operationSymbols.add(leftBracket);
		operationSymbols.add(rightBracket);

		int index = 0;
		boolean findNext = true;
		while (findNext) {
			int nextOperationIndex = expression.length();
			String nextOperation = "";
			// looking for operator or bracket
			for (String operation : operationSymbols) {
				int i = expression.indexOf(operation, index);
				if (i >= 0 && i < nextOperationIndex) {
					nextOperation = operation;
					nextOperationIndex = i;
				}
			}

			if (nextOperationIndex == expression.length()) {
				findNext = false;
			} else {
				// adding operator to out-string if it follows after operand
				if (index != nextOperationIndex) {
					out.add(expression.substring(index, nextOperationIndex));
				}
				// brackets processing
				// opening bracket
				if (nextOperation.equals(leftBracket)) {
					operationsStack.push(nextOperation);
				}
				// closing bracket
				else if (nextOperation.equals(rightBracket)) {
					if (operationsStack.empty()) {
						throw new IllegalArgumentException("Unmatched brackets");
					}
					while (!operationsStack.peek().equals(leftBracket)) {
						out.add(operationsStack.pop());
						if (operationsStack.empty()) {
							throw new IllegalArgumentException(
									"Unmatched brackets");
						}
					}
					operationsStack.pop();
				}
				// operation
				else {
					while (!operationsStack.empty()
							&& !operationsStack.peek().equals(leftBracket)
							&& (operations.get(nextOperation) >= operations
									.get(operationsStack.peek()))) {
						out.add(operationsStack.pop());
					}
					operationsStack.push(nextOperation);
				}
				index = nextOperationIndex + nextOperation.length();
			}
		}

		if (index != expression.length()) {
			out.add(expression.substring(index));
		}

		while (!operationsStack.empty()) {
			out.add(operationsStack.pop());
		}

		if (out.indexOf(leftBracket) != -1 || out.indexOf(rightBracket) != -1) {
			throw new IllegalArgumentException("Unmatched brackets");
		}
		// compiling result RPN string
		StringBuffer result = new StringBuffer();
		if (!out.isEmpty())
			result.append(out.remove(0));
		while (!out.isEmpty())
			result.append(" ").append(out.remove(0));

		return result.toString();
	}

	/**
	 * Converts expression from infix notation to the Reversed Polish Notation
	 * (RPN) using Dijkstra sorting station algorithm
	 * 
	 * @param expression
	 *            infix notation expression
	 * @param operations
	 *            operators that can be used in expression. Each map element
	 *            contains a pair "representation" - "operation priority". The
	 *            highest priority is 1, the next is 2 and so on.
	 * 
	 *            All available operators contained in
	 *            {@link #MAIN_MATH_OPERATIONS}.
	 * @return RPN representation of expression
	 */
	public static String sortingStation(String expression,
			Map<String, Integer> operations) {
		return sortingStation(expression, operations, "(", ")");
	}

	/**
	 * Converts expression from infix notation to the Reversed Polish Notation
	 * (RPN) using Dijkstra sorting station algorithm with default math
	 * operations and brackets
	 * 
	 * @param expression
	 *            infix notation expression
	 * @return
	 */
	public static String sortingStation(String expression) {
		return sortingStation(expression, MAIN_MATH_OPERATIONS, "(", ")");
	}

	/**
	 * Evaluates passed RPN expression. Math constants pi, e will be replased by
	 * their values from Math class.
	 * 
	 * @param rpn
	 *            RPN expression
	 * @return evaluation result.
	 */
	public static double eval(String rpn) {

		StringTokenizer tokenizer = new StringTokenizer(rpn, " ");
		Stack<Double> stack = new Stack<Double>();
		Double operand2;
		Double operand1;
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			// operand
			if (!MAIN_MATH_OPERATIONS.keySet().contains(token)) {
				stack.push(Double.parseDouble(token));
			} else {
				if (token.equals("sin") || token.equals("cos")
						|| token.equals("tg") || token.equals("ctg")) {
					operand2 = stack.pop();
					operand1 = 0.0;
				} else {
					operand2 = stack.pop();
					operand1 = stack.empty() ? 0 : stack.pop();
				}
				if (token.equals("*")) {
					stack.push(operand1 * operand2);
				} else if (token.equals("/")) {
					stack.push(operand1 / operand2);
				} else if (token.equals("+")) {
					stack.push(operand1 + operand2);
				} else if (token.equals("-")) {
					stack.push(operand1 - operand2);
				} else if (token.equals("sin")) {
					stack.push((Math.sin(operand2)));
				} else if (token.equals("cos")) {
					stack.push((Math.cos(operand2)));
				} else if (token.equals("tg")) {
					stack.push((Math.tan((operand2))));
				} else if (token.equals("ctg")) {
					stack.push((1.0 / Math.tan((operand2))));
				} else if (token.equals("^")) {
					stack.push(Math.pow(operand1, operand2));
				}
			}
		}
		if (stack.size() != 1)
			throw new IllegalArgumentException("Syntax error!");
		return stack.pop();
	}

	public static double eval(String rpn, Double x) {
		rpn = rpn.replaceAll("x", "" + x);
		return eval(rpn);
	}

	/**
	 * Class instances creation is forbidden
	 */
	private FunctionEvaluator() {
	}

}
