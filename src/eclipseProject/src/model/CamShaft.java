package model;

import java.util.ArrayList;

/**
 * Cam entity that contain type, movement trajectory, profile and methods for it 
 * calculation
 *
 */
public class CamShaft {
	
	private CamShaftType type;
	private ArrayList<Point> trajectory;
	private ArrayList<Point> profile;
	
	private CamShaftCalculator camshaftCalculator;
	
	public CamShaft(CamShaftType type) {
		this.type = type;
		profile = new ArrayList<Point>();
		trajectory = new ArrayList<Point>();
	}
	
	public void calculateProfile(double pres) {
		profile = camshaftCalculator.calculateProfile(trajectory, pres);
	}
	
	/**
	 * @return the profile
	 */
	public ArrayList<Point> getProfile() {
		return profile;
	}

	/**
	 * @param profile the profile to set
	 */
	public void setProfile(ArrayList<Point> profile) {
		this.profile = profile;
	}

	/**
	 * @return the trajectory
	 */
	public ArrayList<Point> getTrajectory() {
		return trajectory;
	}
	
	/**
	 * @param trajectory the trajectory to set
	 */
	public void setTrajectory(ArrayList<Point> trajectory) {
		this.trajectory = trajectory;
	}
	
	/**
	 * @return the type
	 */
	public CamShaftType getType() {
		return type;
	}
	
	/**
	 * @return the camshaftCalculator
	 */
	public CamShaftCalculator getCamshaftCalculator() {
		return camshaftCalculator;
	}

	/**
	 * @param camshaftCalculator the camshaftCalculator to set
	 */
	public void setCamshaftCalculator(CamShaftCalculator camshaftCalculator) {
		this.camshaftCalculator = camshaftCalculator;
	}

}
