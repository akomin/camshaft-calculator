package model;

import java.util.HashMap;

/**
 * Calculates profile for CamShaftType.TRANSICTING type based cam
 *
 */
public class TransictingCamShaftCalculator extends AnaliticCamShaftCalculator {
	
	public TransictingCamShaftCalculator(HashMap<String,Double> props){
		X = props.get("X");
		d = props.get("d");
		Rmin = props.get("Rmin");		
	}

	@Override
	protected double getFn(double r) {
		double Ymin = Math.sqrt(r * r + X * X);
		double q = 0;
		if (X < 0)
			q = Math.PI;
		return (q +  Math.atan(Ymin / X));
	}

	@Override
	protected double getRi(double Sbi) {
		double S0 = Math.sqrt(Rmin * Rmin + X * X);
		return Math.sqrt((S0 + Sbi) * (S0 + Sbi) + X * X);
	}


}
