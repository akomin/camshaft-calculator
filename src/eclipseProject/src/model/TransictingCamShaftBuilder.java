package model;

import java.util.HashMap;

/**
 * Builds CamShaft objects with CCamShaftType.TRANSICTING type and specific profile calculator class
 * TransictingCamShaftCalculator
 */
public class TransictingCamShaftBuilder extends CamShaftBuilder {

	@Override
	public void build(HashMap<String, Double> props) {
		this.camshaft = new CamShaft(CamShaftType.TRANSICTING);
		this.camshaft.setCamshaftCalculator(new TransictingCamShaftCalculator(props));
	}

	@Override
	public void reinit(CamShaft camshaft, HashMap<String, Double> props) {
		camshaft.setCamshaftCalculator(new TransictingCamShaftCalculator(props));
	}

}
