package model;

import java.util.HashMap;

/**
 * Class that returns CamShaft objects with specific CamShaftCalculator instance based
 * on passed cam type.
 */
public class CamShaftFactory {
	
	/**
	 * Returns CamShaft object based on passed type and initializes it with passed parameters
	 * @param type type of cam
	 * @param props map with "parameter" - "value" to initialize
	 * @return initialized CamShaft object
	 */
	public static CamShaft create(CamShaftType type, HashMap<String,Double> props) {
		
		CamShaftBuilder builder = null;
		
		switch(type) {
		case TRANSICTING :
			builder = new TransictingCamShaftBuilder();
			builder.build(props);
			return builder.getCamShaft();

		case OSCILATING :
			builder = new OsciliatingCamShaftBuilder();
			builder.build(props);
			return builder.getCamShaft();

		default : throw new IllegalArgumentException("Unknown camshaft type passed");
		}
			
	}
	
	/**
	 * Reinitialize passed CamShaft object with new parameters
	 * @param camShaft object to initialize
	 * @param props map with "parameter" - "value" to initialize 
	 */
	public static void reinit(CamShaft camShaft,
			HashMap<String, Double> props) {

		CamShaftBuilder builder = null;
		CamShaftType type = camShaft.getType();
		
		switch(type) {
		case TRANSICTING :
			builder = new TransictingCamShaftBuilder();
			builder.reinit(camShaft, props);
		case OSCILATING :
			builder = new OsciliatingCamShaftBuilder();
			builder.reinit(camShaft, props);
		default : throw new IllegalArgumentException("Unknown camshaft type passed"); 
		} 
	}
}
