package model;

import java.util.ArrayList;

/**
 * Abstract class for calculators that calculates CamShaft profiles 
 * by specific algorithms based on cam type.
 */
public interface CamShaftCalculator {
	
	/**
	 * Calculates cam profile based on movement trajectory and passed precision
	 * @param trajectory points obtained from cam's movement law
	 * @param pres - precision of profile calculation
	 * @return
	 */
	ArrayList<Point> calculateProfile(ArrayList<Point> trajectory, double pres);
	
}
