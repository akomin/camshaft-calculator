package model;

import java.util.HashMap;

/**
 * Calculates profile for CamShaftType.OSCILATING type based cam
 *
 */
public class OsciliatingCamShaftCalculator extends AnaliticCamShaftCalculator {
	
	public OsciliatingCamShaftCalculator(HashMap<String,Double> props){
		X = props.get("X");
		Y = props.get("Y");
		L = props.get("L");
		d = props.get("d");
		Rmin = props.get("Rmin");		
	}
	
	@Override
	protected double getFn(double r) {
		return Math.acos(X / l) + Math.acos((r * r + l* l - L * L)/ (2 *r * l));
	}
	@Override
	protected double getRi(double Sbi) {
		return Math.sqrt(l * l + L * L - 2 * l * L
				* Math.cos(Fl0 + Sbi * Math.PI / 180));
	}

}
