package model;

import java.util.HashMap;

/**
 * Abstract class for builders that creates CamShaft object based on passed CamSShaftType
 */
public abstract class CamShaftBuilder {
	
	protected CamShaft camshaft;
	
	/**
	 * Returns CamShaft object. Must be build by build method previously. 
	 * @return CamShaft object
	 */
	public CamShaft getCamShaft() { 
		return this.camshaft;
	}
	
	/**
	 * Builds CamShaft object and initializes it with passes parameters
	 * @param props map with "parameter" - "value" to initialize CamShaft fields
	 */
	public abstract void build(HashMap<String,Double> props);
	
	/**
	 * Reinitialize passed CamShaft object with passed parameters
	 * @param camshaft object for reinitialization
	 * @param props map with "parameter" - "value" to initialize object fields
	 */
	public abstract void reinit(CamShaft camshaft, HashMap<String,Double> props);
	
	

}
